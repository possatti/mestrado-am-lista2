from setuptools import setup

setup(name='am',
      version='0.1',
      description='Collection of modules for machine learning.',
      url='https://gitlab.com/possatti/mestrado-am-lista1',
      author='Lucas Possatti',
      author_email='lucas.cpossatti@gmail.com',
      license='MIT',
      packages=['am'],
      zip_safe=False)
