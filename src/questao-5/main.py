#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from am.classification import DecisionTree, tree_to_graphviz

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

def main():
	# Fix PRNG seed for reproducibility.
	np.random.seed(7)

	# Load data.
	dataset_str = np.loadtxt(args.car_dataset, dtype=bytes, delimiter=',').astype(str)

	# Convert strings to ints.
	encoders = []
	dataset = np.empty(shape=dataset_str.shape, dtype=int)
	for a in range(dataset_str.shape[1]):
		encoder = LabelEncoder()
		dataset[:,a] = encoder.fit_transform(dataset_str[:,a])
		encoders.append(encoder)

	# Shuffle the dataset.
	np.random.shuffle(dataset)

	# Split data from labels.
	data = dataset[:,:-1]
	labels = dataset[:,-1]

	# Create model.
	if args.sk:
		estimator = DecisionTreeClassifier(max_depth=args.max_depth)
	else:
		estimator = DecisionTree(max_depth=args.max_depth)

	# Train and evaluate model.
	train_data, test_data, train_labels, test_labels = train_test_split(data, labels, test_size=0.25)
	estimator.fit(train_data, train_labels)
	# predictions = estimator.predict(test_data)#!#
	# print('predictions:', predictions, file=sys.stderr)#!#
	acc = estimator.score(test_data, test_labels)
	print('Accuracy:', acc)

	# Show enconding.
	print('\nEncondings:')
	attr_names = ['buying', 'maint', 'doors', 'persons', 'lug_boot', 'safety', 'class']
	for attr, name in enumerate(attr_names):
		numeric = values = np.unique(dataset[:,attr])
		str_values = encoders[attr].inverse_transform(values)
		attr_dict = dict(zip(numeric, str_values))
		print('({}) {:<9} => '.format(attr, name), '  '.join([ '{:<1}: {:<5}'.format(n, s) for n, s in attr_dict.items() ]))

	# Export the undelying structure of the tree.
	if args.create_visualization:
		dot_path = os.path.join(os.path.dirname(__file__), 'tree.dot')
		png_path = os.path.join(os.path.dirname(__file__), 'tree.png')
		if args.sk:
			export_graphviz(estimator.tree_,  dot_path)
			call(['dot', '-Tpng', dot_path, '-o', png_path])
		else:
			root = estimator.root_node_
			dot_content = tree_to_graphviz(root)
			with open(dot_path, 'w') as f:
				f.write(dot_content)
			call(['dot', '-Tpng', dot_path, '-o', png_path])


if __name__ == '__main__':
	# Arguments and options
	default_car_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'car.data'))
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--car-dataset', metavar='FILE_PATH', default=default_car_dataset_path)
	parser.add_argument('--sk', action='store_true', help='Use estimator from Scikit Learn.')
	parser.add_argument('-d', '--max-depth', type=int, default=2, help='The decision tree maximum depth.')
	parser.add_argument('-g', '--create-visualization', action='store_true', help='Create visualization using graphviz.')
	args = parser.parse_args()
	main()
