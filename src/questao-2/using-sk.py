#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.naive_bayes import GaussianNB, MultinomialNB

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)
print('Numpy version:', np.version.version, file=sys.stderr)

def main():
	dataset = np.loadtxt(args.balance_dataset, dtype=bytes, delimiter=',')
	np.random.seed(7)
	np.random.shuffle(dataset)
	labels = dataset[:,0].astype(str)
	data = dataset[:,1:].astype(int)

	train_data, test_data, train_labels, test_labels = train_test_split(data, labels, test_size=0.25)
	kfold = KFold(n_splits=5)

	estimators = { 'gaussian': GaussianNB(), 'multinomial': MultinomialNB()}
	for name, estimator in estimators.items():
		print('Using {}'.format(name))

		# Hold-out.
		estimator.fit(train_data, train_labels)
		score = estimator.score(test_data, test_labels)
		print('Holdout accuracy:', score)

		# KFold.
		accs = cross_val_score(estimator, data, labels, cv=kfold)
		print('KFold accuracies:', accs)
		print('Kfold mean acurracy:', accs.mean())
		print()



if __name__ == '__main__':
	# Arguments and options
	default_balance_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'balance-scale.data'))
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--balance-dataset', metavar='file_path', default=default_balance_dataset_path)
	args = parser.parse_args()
	main()
