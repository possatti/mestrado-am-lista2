#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.classification import GaussianNaiveBayes, DiscreteNaiveBayes
from sklearn.model_selection import cross_val_score, train_test_split, KFold

# import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)
print('Numpy version:', np.version.version, file=sys.stderr)

def main():
	dataset = np.loadtxt(args.balance_dataset, dtype=bytes, delimiter=',')
	np.random.seed(7)
	np.random.shuffle(dataset)
	labels = dataset[:,0].astype(str)
	data = dataset[:,1:].astype(int)

	if args.letter_a:
		print('Assuming Gaussian distribution for attributes...')
		estimator = GaussianNaiveBayes()
	elif args.letter_b:
		print('Using Naive Bayes assuming discrete values...')
		estimator = DiscreteNaiveBayes(alpha=0)
	elif args.letter_c:
		print('Using Naive Bayes and Laplace assuming discrete values...')
		estimator = DiscreteNaiveBayes(alpha=1)
	print()

	# Simple hold-out validation
	print('Doing 10 hold-outs.')
	accs = np.empty(10)
	for i in range(10):
		train_data, validation_data, train_labels, validation_labels = train_test_split(data, labels, test_size=0.2)
		estimator.fit(train_data, train_labels)
		predictions = estimator.predict(validation_data)
		acc = (predictions==validation_labels).sum() / len(validation_labels)
		accs[i] = acc
		# print('10 predicions:', predictions[:10])
		# print('10 labels:    ', validation_labels[:10])
	print('10 Hold-outs accuracies:', accs)
	print('10 Hold-outs mean accuracy:', accs.mean())
	print('10 Hold-outs standard deviation:', accs.std())
	print()

	# Cross validation.
	n_splits = args.kfold
	print('Doing k-fold cross-validation with {} folds.'.format(n_splits))
	kfold = KFold(n_splits=n_splits, shuffle=True)
	accs = cross_val_score(estimator, data, labels, cv=kfold)
	print('Accuracies from the cross-validation:\n', accs)
	print('Mean accuracy from cross-validation:\n', accs.mean())


if __name__ == '__main__':
	# Arguments and options
	default_balance_dataset_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'balance-scale.data'))

	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--balance-dataset', metavar='file_path', default=default_balance_dataset_path)
	parser.add_argument('-k', '--kfold', metavar='K', type=int, default=10)

	letter_group = parser.add_mutually_exclusive_group(required=True)
	letter_group.add_argument('-a', '--letter-a', action='store_true')
	letter_group.add_argument('-b', '--letter-b', action='store_true')
	letter_group.add_argument('-c', '--letter-c', action='store_true')

	args = parser.parse_args()

	main()
