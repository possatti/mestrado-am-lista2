#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

def main():
	dataset = np.loadtxt(args.car_evaluation_dataset, delimiter=',', dtype=bytes).astype(str)

	##### A #####
	print('a) P(x1 = med) e P(x2 = low)')
	print(' - P(x1 = med) =', (dataset[:,0]=='med').sum() / len(dataset))
	print(' - P(x2 = low) =', (dataset[:,1]=='low').sum() / len(dataset))
	print()

	##### B #####
	print('b) P(x6=high|x3=2) e P(x2=low|x4=4)')
	x3eq2 = dataset[:,2]=='2'
	x6eqhigh = dataset[:,5]=='high'
	x3eq2_marginal = x3eq2.sum() / len(dataset)
	x6eqhigh_marginal = x6eqhigh.sum() / len(dataset)
	print(' - P(x6=high|x3=2) =', (x6eqhigh_marginal * x3eq2_marginal) / x3eq2_marginal, '(using marginal probabilities)')
	print(' - P(x6=high|x3=2) =', (dataset[x3eq2,5]=='high').sum() / x3eq2.sum())

	x4eq4 = dataset[:,3]=='4'
	x2eqlow = dataset[:,1]=='low'
	x4eq4_marginal = x4eq4.sum() / len(dataset)
	x2eqlow_marginal = x2eqlow.sum() / len(dataset)
	print(' - P(x2=low|x4=4) =', (x2eqlow_marginal * x4eq4_marginal) / x4eq4_marginal, '(using marginal probabilities)')
	print(' - P(x2=low|x4=4) =', (dataset[x4eq4,1]=='low').sum() / x4eq4.sum())
	print()

	##### C #####
	print('c) P(x1=low|x2=low,X5=small) e P(x4=4|x1=med,X3=2)')
	x1eqmed = dataset[:,0] == 'med'
	x3eq2 = dataset[:,2] == '2'
	x1eqmed_marginal = x1eqmed.sum() / len(dataset)
	x3eq2_marginal = x3eq2.sum() / len(dataset)
	print(' - P(x4=4|x1=med,X3=2) =', (x4eq4_marginal * x1eqmed_marginal * x3eq2_marginal) / (x1eqmed_marginal * x3eq2_marginal), '(using marginal probabilities)')

	x1eqlow = dataset[:,0] == 'low'
	x2eqlow = dataset[:,1] == 'low'
	x5eqsmall = dataset[:,4] == 'small'
	x1eqlow_marginal = x1eqlow.sum() / len(dataset)
	x2eqlow_marginal = x2eqlow.sum() / len(dataset)
	x5eqsmall_marginal = x5eqsmall.sum() / len(dataset)
	print(' - P(x1=low|x2=low,X5=small) =', (x1eqlow_marginal*x2eqlow_marginal*x5eqsmall_marginal) / (x2eqlow_marginal*x5eqsmall_marginal), '(using marginal probabilities)')
	print()

	##### D #####
	print('d) P(x2=vhigh,X3=2|X4=2) e P(x3=4,x5=med|x1=med)')

	x4eq2 = dataset[:,3] == '2'
	x2eqvhigh_and_x3eq2_given_x4eq2 = np.logical_and(dataset[x4eq2,1]=='vhigh', dataset[x4eq2,2]=='2')
	print(' - P(x2=vhigh,X3=2|X4=2) = ', x2eqvhigh_and_x3eq2_given_x4eq2.sum() / x4eq2.sum())

	x1eqmed = dataset[:,0] == 'med'
	x3eq4_and_x5eqmed_given_x1eqmed = np.logical_and(dataset[x1eqmed,2]=='4', dataset[x1eqmed,4]=='med')
	print(' - P(x3=4,x5=med|x1=med) = ', x3eq4_and_x5eqmed_given_x1eqmed.sum() / x1eqmed.sum())


if __name__ == '__main__':
	# Arguments and options
	default_car_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'car.data'))
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--car-evaluation-dataset', metavar='file_path', default=default_car_path, help='Path to the Car Evaluation dataset.')
	args = parser.parse_args()
	main()
