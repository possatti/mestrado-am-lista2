import numpy as np
import math

def linear_regression(X, t):
	"""Estimates a linear model that maps `x` to `t`."""
	w = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(t)
	return w
