import numpy as np
import math

def find_centroid(instances):
	"""Calculate centroid for the given instances."""
	coordinates = []
	for feat_index in range(instances.shape[1]):
		coordinates.append(instances[:,feat_index].mean())
	return np.array(coordinates)

def euclidean_distance(a, b):
	"""Euclidean distance calculation."""
	assert a.shape == b.shape, 'Both instances need the same dimensions. {} != {}'.format(a.shape, b.shape)
	assert len(a.shape) == 1, 'Each instance must be an array.'
	return math.sqrt(((a - b) ** 2 ).sum())

def cosine_similarity(a, b):
	"""Cosine similarity calculation."""
	assert a.ndim == 1 and b.ndim == 1, 'The two parameters must be vectors (nº dim.: {}, {}).'.format(a.ndim, b.ndim)
	assert a.shape == b.shape, 'The two parameters must have the same length ({} != {})'.format(a.shape, b.shape)
	mag_a = math.sqrt((a**2).sum())
	mag_b = math.sqrt((b**2).sum())
	inner_prod = (a*b).sum()
	return inner_prod / (mag_a * mag_b)

def mean(values):
	"""Calculate mean from the given values."""
	return sum(values) / len(values)

def variance(a):
	"""Calculate the variance of the given array."""
	mean = sum(a)/len(a)
	variance = sum( (a - mean) ** 2 ) / len(a)
	return variance

def covariance(a, b):
	"""Calculate the covariance between two arrays."""
	mean_a = sum(a)/len(a)
	mean_b = sum(b)/len(b)
	covariance = sum( (a - mean_a) * (b - mean_b) ) / len(a)
	return covariance

def covariance_matrix(data, rowvar=True):
	"""Builds a covariance matrix from the data. Each row is a variable if `rowvar`."""
	if not rowvar:
		data = data.transpose()
	dims = data.shape[0] # Rows numbers (number of variables).
	cov_matrix = np.zeros(shape=(dims, dims))
	for row in range(dims):
		for col in range(dims):
			if row == col:
				cov_matrix[row,col] = variance(data[row,:])
			else:
				cov_matrix[row,col] = covariance(data[row], data[col])
	return cov_matrix

def pearson(a, b):
	"""Calculate pearson's coefficient for the two arrays."""
	return covariance(a, b) / (a.std() * b.std())

def correlation_matrix(data, rowvar=True):
	"""Builds a correlation matrix from the data. Each row is a variable if `rowvar`."""
	if not rowvar:
		data = data.transpose()
	dims = data.shape[0] # Rows numbers (number of variables).
	corr_matrix = np.zeros(shape=(dims, dims))
	for row in range(dims):
		for col in range(dims):
			corr_matrix[row,col] = pearson(data[row], data[col])
	return corr_matrix

def confusion_matrix(labels, ground_truth, possible_labels):
	conf_matrix = np.zeros(shape=(len(possible_labels), len(possible_labels)))
	for label, gt in zip(labels, ground_truth):
		label_index = possible_labels.index(label)
		gt_index = possible_labels.index(gt)
		conf_matrix[gt_index, label_index] += 1
	return conf_matrix

def root_mean_square_error(x, y, fx):
	"""RMSE."""
	return math.sqrt(((fx - y)**2).mean())

def mean_absolute_percentage_error(x, y, fx):
	"""MAPE."""
	return np.abs((y - fx) / y).mean()

def coefficient_of_determination(x, y, fx):
	"""Calculate R^2."""
	numerator = ((y - fx) ** 2).sum()
	denominator = ((y - y.mean()) ** 2).sum()
	return 1 - numerator / denominator
