from sklearn.base import BaseEstimator, ClassifierMixin
import numpy as np
import math

OPERERATIONS = {
	'<': lambda a, b: a < b,
	'==': lambda a, b: a == b,
	'>': lambda a, b: a > b,
}

def entropy(A):
	values_unique, counts = np.unique(A, return_counts=True)
	ps = counts / A.size
	ps = ps[ps != 0]
	return (-ps * np.log2(ps)).sum()

def information_gain(A, partitions):
	sizes = np.array([ partition.size for partition in partitions ])
	entropies = np.empty(shape=len(partitions), dtype=float)
	for i, partition in enumerate(partitions):
		entropies[i] = entropy(partition)
	ig = entropy(A) - ( entropies * (sizes / sizes.sum()) ).sum()
	return ig

class DecisionNode:
	def __init__(self, attr_index, operator, value, children=None):
		self.attr_index = attr_index
		self.operator = operator
		self.value = value
		self.children = children or []

	def append(self, child):
		self.children.append(child)

	def split(self, instances):
		trues = OPERERATIONS[self.operator](instances[:,self.attr_index], self.value)
		falses = np.logical_not(trues)
		return trues, falses

	def predict_one(self, instance):
		test_result = OPERERATIONS[self.operator](instance[self.attr_index], self.value)
		if test_result:
			return self.children[1].predict_one(instance)
		else:
			return self.children[0].predict_one(instance)

	def predict(self, instances):
		return np.array([ self.predict_one(instance) for instance in instances ])

	def __hash__(self):
		return hash((self.attr_index, self.operator, self.value))

class BinaryDecisionNode:
	def __init__(self, attr_index, operator, value):
		self.attr_index = attr_index
		self.operator = operator
		self.value = value
		self.true_child = None
		self.false_child = None

	def split(self, instances):
		"""Split instances according to this node's operation."""
		trues = OPERERATIONS[self.operator](instances[:,self.attr_index], self.value)
		falses = np.logical_not(trues)
		return trues, falses

	def predict_one(self, instance):
		test_result = OPERERATIONS[self.operator](instance[self.attr_index], self.value)
		if test_result:
			return self.true_child.predict_one(instance)
		else:
			return self.false_child.predict_one(instance)

	def predict(self, instances):
		return np.array([ self.predict_one(instance) for instance in instances ])

	def __hash__(self):
		return hash((self.attr_index, self.operator, self.value))

class LeafNode:
	def __init__(self, label):
		self.label = label

	def predict_one(self, instance):
		return self.label

	def predict(self, instances):
		return np.repeat(self.label, len(instances))

	def __hash__(self):
		return hash(self.label)


def find_best_node(data, labels):
	nodes_and_gains = {}
	for attr in range(data.shape[1]):
		values_unique, counts = np.unique(data[:,attr], return_counts=True)
		cut_values = [ np.array([values_unique[i], values_unique[i+1]]).mean() for i in range(len(values_unique) - 1) ]
		# print('values_unique:', values_unique)#!#
		# print('cut_values:', cut_values)#!#
		for cut in cut_values:
			# Split using `attribute < cut`.
			node = BinaryDecisionNode(attr, '<', cut)
			trues, falses = node.split(data)
			ig = information_gain(labels, [labels[trues], labels[falses]])
			nodes_and_gains[node] = ig

	if len(nodes_and_gains) > 0:
		best_node = max(nodes_and_gains, key=nodes_and_gains.get)
		return best_node
	else:
		# If we can't split any attribute anymore, we create a leaf node with
		# the prevailing label.
		labels_unique, counts = np.unique(labels, return_counts=True)
		majority_label = labels_unique[np.argmax(counts)]
		return LeafNode(majority_label)

def build_tree_recursive(data, labels, max_depth=None):
	if max_depth == 0:
		# Choose the most frequent label, and use that for the leaf.
		labels_unique, counts = np.unique(labels, return_counts=True)
		label = labels_unique[np.argmax(counts)]
		return LeafNode(label)
	else:
		node = find_best_node(data, labels)
		groups = node.split(data)
		print('len(groups):', len(groups))
		for group in groups:
			if group.sum() == group.size: # All of them are true.
				label = labels[group][0]
				print('labels[group]:', labels[group])
				node.append(LeafNode(label))
			else:
				if max_depth is not None:
					subtree = build_tree(data[group], labels[group], max_depth=max_depth-1)
					print('type(subtree)',type(subtree))
					node.append(subtree)
				else:
					# print('Dont print this !!!!!!!!!!!!!!!!!!!!!!!!!!')#!#
					node.append(build_tree(data[group], labels[group]))
		print('len(node.children):', len(node.children))
		return node

def build_tree_iterative(data, labels, max_depth=None):
	root = find_best_node(data, labels)
	previous_nodes  = [root]
	previous_datas  = [data]
	previous_labels = [labels]
	for depth in range(max_depth):
		current_nodes  = []
		current_datas  = []
		current_labels = []
		for prev_node, prev_data, prev_labels in zip(previous_nodes, previous_datas, previous_labels):
			groups = prev_node.split(prev_data)
			for group in groups:
				if group.sum() == group.size:
					labels_unique = np.unique(prev_labels[group])
					assert len(labels_unique)==1, 'You can\'t create a leaf node yet!'
					label, = labels_unique
					leaf = LeafNode(label)
					prev_node.append(leaf)
				else:
					node = find_best_node(prev_data[group], prev_labels[group])
					prev_node.append(node)
					current_nodes.append(node)
					current_datas.append(prev_data[group])
					current_labels.append(prev_labels[group])
		# print('Previous hashes:', [ hash(node) for node in previous_nodes ])#!#
		# print('Current hashes:', [ hash(node) for node in current_nodes ])#!#
		previous_nodes  = current_nodes
		previous_datas  = current_datas
		previous_labels = current_labels
	else:
		# print('Previous hashes:', [ hash(node) for node in previous_nodes ])#!#
		# print('Current hashes:', [ hash(node) for node in current_nodes ])#!#
		for prev_node, prev_data, prev_labels in zip(previous_nodes, previous_datas, previous_labels):
			groups = prev_node.split(prev_data)
			for group in groups:
				labels_unique, counts = np.unique(prev_labels[group], return_counts=True)
				majority_label = labels_unique[np.argmax(counts)]
				leaf = LeafNode(majority_label)
				prev_node.append(leaf)
	return root

def build_binary_tree_iterative(data, labels, max_depth=None):
	root = find_best_node(data, labels)
	previous_nodes  = [root]
	previous_datas  = [data]
	previous_labels = [labels]
	depth = 1
	while True:
		current_nodes  = []
		current_datas  = []
		current_labels = []
		for prev_node, prev_data, prev_labels in zip(previous_nodes, previous_datas, previous_labels):
			if type(prev_node) is LeafNode:
				continue
			groups = prev_node.split(prev_data)
			for g, group in enumerate(groups):
				if group.sum() == group.size:
					labels_unique = np.unique(prev_labels[group])
					assert len(labels_unique)==1, 'You can\'t create a leaf node yet!'
					label, = labels_unique
					node = LeafNode(label)
				else:
					node = find_best_node(prev_data[group], prev_labels[group])
					current_nodes.append(node)
					current_datas.append(prev_data[group])
					current_labels.append(prev_labels[group])
				if g == 0:
					prev_node.true_child = node
				elif g == 1:
					prev_node.false_child = node
		previous_nodes  = current_nodes
		previous_datas  = current_datas
		previous_labels = current_labels
		depth += 1
		if max_depth is not None and depth >= max_depth:
			break

	if max_depth is not None:
		for prev_node, prev_data, prev_labels in zip(previous_nodes, previous_datas, previous_labels):
			if type(prev_node) is LeafNode:
				continue
			groups = prev_node.split(prev_data)
			for g, group in enumerate(groups):
				labels_unique, counts = np.unique(prev_labels[group], return_counts=True)
				majority_label = labels_unique[np.argmax(counts)]
				leaf = LeafNode(majority_label)
				if g == 0:
					prev_node.true_child = leaf
				elif g == 1:
					prev_node.false_child = leaf
	return root


class DecisionTree(BaseEstimator, ClassifierMixin):
	"""Classifier based on Decision Trees."""
	def __init__(self, max_depth=None):
		self.max_depth = max_depth

	def fit(self, data, labels):
		self.root_node_ = build_binary_tree_iterative(data, labels, max_depth=self.max_depth)

	def predict(self, data):
		return self.root_node_.predict(data)

def tree_to_graphviz(root):
	node_queue = [root]
	nodes, edges = {}, []
	while len(node_queue) > 0:
		current_node = node_queue.pop()
		if type(current_node) == BinaryDecisionNode:
			nodes[hash(current_node)] = '[label="X[{attr_index}] {operator} {value}"]'.format(**current_node.__dict__)
			edges.append('{} -> {} [label="True"]'.format(hash(current_node), hash(current_node.true_child)))
			edges.append('{} -> {} [label="False"]'.format(hash(current_node), hash(current_node.false_child)))
			node_queue += [current_node.true_child, current_node.false_child]
		elif type(current_node) == DecisionNode:
			nodes[hash(current_node)] = '[label="X[{attr_index}] {operator} {value}"]'.format(**current_node.__dict__)
			for child in current_node.children:
				edges.append('{} -> {} [label=""]'.format(hash(current_node), hash(child)))
			node_queue += current_node.children
		elif type(current_node) == LeafNode:
			nodes[hash(current_node)] = '[shape="circle", label="class = {label}"]'.format(**current_node.__dict__)

	content = 'digraph Tree {\nnode [shape=box] ;\n'
	for node_hash, stuff in nodes.items():
		content += '{} {}\n'.format(node_hash, stuff)
	for edge in edges:
		content += edge + '\n'
	content += '}\n'
	return content
