from am.utils import euclidean_distance, cosine_similarity, find_centroid
from functools import partial
import numpy as np

class NearestNeighbor(object):
	"""Nearest neighbor classification algorithm."""
	EUCLIDEAN_DISTANCE = 1
	COSINE_SIMILARITY = 2

	def train(self, instances, labels):
		self.training_instances = instances
		self.training_labels = labels

	def predict_one(self, instance, method=EUCLIDEAN_DISTANCE):
		"""Predict label for a single instance."""
		if method == NN.EUCLIDEAN_DISTANCE:
			distances = []
			for t_instance in self.training_instances:
				distances.append(euclidean_distance(instance, t_instance))
			closest_index = np.argmin(distances)
			preditected_label = self.training_labels[closest_index]
		elif method == NN.COSINE_SIMILARITY:
			similarities = []
			for t_instance in self.training_instances:
				similarities.append(cosine_similarity(instance, t_instance))
			closest_index = np.argmax(similarities)
			preditected_label = self.training_labels[closest_index]
		else:
			raise ValueError('You must pick a valid method.')
		return preditected_label

	def predict(self, instances, method=EUCLIDEAN_DISTANCE):
		"""Predict label for multiple instances."""
		predict = partial(self.predict_one, method=method) # Binds the specified method.
		return np.asarray(list(map(predict, instances)))
