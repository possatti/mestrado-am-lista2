from am.utils import euclidean_distance, cosine_similarity, find_centroid
from functools import partial
import numpy as np

class Rocchio(object):
	"""Rocchio classification algorithm."""

	def train(self, instances, labels):
		self.instances = instances
		self.labels = labels
		self.centroids = {}
		labels_unique = np.unique(labels)
		for label in labels_unique:
			this_label_instances = instances[labels == label]
			self.centroids[label] = find_centroid(this_label_instances)

	def predict_one(self, instance):
		"""Predict label for a single instance."""
		distances = np.ones(len(self.centroids))
		for i, centroid in enumerate(self.centroids.values()):
			distances[i] = euclidean_distance(instance, centroid)
		return list(self.centroids.keys())[distances.argmin()]

	def predict(self, instances):
		"""Predict label for multiple instances."""
		return np.asarray(list(map(self.predict_one, instances)))
