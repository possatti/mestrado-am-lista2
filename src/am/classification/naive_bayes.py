from sklearn.utils.estimator_checks import check_estimator
from sklearn.base import BaseEstimator, ClassifierMixin

import numpy as np
import math


class GaussianNaiveBayes(BaseEstimator, ClassifierMixin):
	"""Naive Bayes classification algorithm for continuous attributes."""
	def fit(self, data, labels):
		self.labels_unique = np.unique(labels)
		self.means, self.stds = [], []
		for label in self.labels_unique:
			label_data = data[label == labels]
			self.means.append(label_data.mean(axis=0))
			self.stds.append(label_data.std(axis=0))

	def calculateProbability(self, x, mean, stdev):
		exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
		return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent

	def predict(self, data):
		predictions = []
		for instance in data:
			labels_probs = []
			for l, label in enumerate(self.labels_unique):
				probs = []
				for v, value in enumerate(instance):
					probs.append(self.calculateProbability(value, self.means[l][v], self.stds[l][v]))
				label_prob = probs[0]
				for prob in probs[1:]:
					label_prob *= prob
				labels_probs.append(label_prob)
			best_label_index = np.argmax(labels_probs)
			predicted_label = self.labels_unique[best_label_index]
			predictions.append(predicted_label)
		return np.array(predictions)

class DiscreteNaiveBayes(BaseEstimator, ClassifierMixin):
	"""Naive Bayes classification algorithm for discrete attributes."""
	def __init__(self, alpha=1):
		"""Creates a new instance of the estimator.

		Arguments
		---------
		alpha -- Additive Laplace smoothing (0 = no smoothing). Default is 1.
		"""
		self.alpha = alpha

	def fit(self, data, labels):
		self.labels_unique = np.unique(labels)
		self.attr_n = data.shape[1]
		self.summary = np.empty(shape=(len(self.labels_unique), data.shape[1]), dtype=object)
		for l, label in enumerate(self.labels_unique):
			label_data = data[label == labels]
			for a, attribute_vector in enumerate(zip(*label_data)):
				values, counts = np.unique(attribute_vector, return_counts=True)
				self.summary[l,a] = dict(zip(values, counts))
		return self

	def predict(self, data):
		predictions = []
		for instance in data:
			labels_probs = []
			for l, label in enumerate(self.labels_unique):
				label_probability = None # Cumulative probability that the instance belongs to this class.
				for v, value in enumerate(instance):
					attr_summary = self.summary[l,v]

					# P(value|label)
					if self.alpha > 0:
						value_probability = attr_summary[value]+self.alpha / sum(attr_summary.values())+self.attr_n*self.alpha
					else:
						value_probability = attr_summary[value] / sum(attr_summary.values())

					if label_probability is None:
						label_probability = value_probability
					else:
						label_probability *= value_probability
				labels_probs.append(label_probability)
			max_prob_label_index = np.argmax(labels_probs)
			prediction = self.labels_unique[max_prob_label_index]
			predictions.append(prediction)
		return np.array(predictions)


if __name__ == '__main__':
	check_estimator(GaussianNaiveBayes)
	check_estimator(DiscreteNaiveBayes)
