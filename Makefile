#!/usr/bin/make -f
SHELL=/bin/bash
.ONESHELL:
.PHONY: all clean q1a q1b q1c q1d q2a q2b q2c q5a q5b q5c


REPORT_PDF = ./relatorio/relatorio.pdf
REPORT_SOURCES = ./relatorio/relatorio.tex $(shell find ./relatorio/ -regex ".*\.png")
SOURCE_LIST = $(shell find src/ -regex ".*\.\(py\|png\)")
ZIP_PACKAGE = lucas-possatti-lista2.zip


all: $(ZIP_PACKAGE)

$(ZIP_PACKAGE): $(REPORT_PDF) $(SOURCE_LIST) README.md Makefile
	zip -r $(@) $(?)

$(REPORT_PDF): $(REPORT_SOURCES)
	$(MAKE) --directory relatorio

clean:
	rm -f $(ZIP_PACKAGE)


## How to run each question ##
q1a:
	python src/questao-1/main.py
q1b:
	python src/questao-1/main.py
q1c:
	python src/questao-1/main.py
q1d:
	python src/questao-1/main.py

q2a:
	python src/questao-2/main.py -a
q2b:
	python src/questao-2/main.py -b
q2c:
	python src/questao-2/main.py -c

q5a:
	python src/questao-5/main.py
q5b:
	python src/questao-5/main.py
q5c:
	python src/questao-5/main.py
